import logging as log
import configparser
import os
import socket
import threading
import queue
#from memory_tempfile import MemoryTempfile
import sqlite3
from memory_tempfile import MemoryTempfile
import time

### FAQ
# приложение многопоточное
# поток TCP-сервера принимает от Quick данные
# передает данные в поток обработчика (broker) через очередь
# поток брокера пишет данные в базу данных sqlite
# база данных sqlite находится в

### НАСТРОЙКИ
config = configparser.ConfigParser()
config.read('config.ini')
name_db = str(config['CONFIG']['name_db'])
time_commit_db = int(config['CONFIG']['time_commit_db'])
volume_db_str = int(config['CONFIG']['volume_db_str'])
ip = str(config['QUICK']['ip'])
port = int(config['QUICK']['port'])

### ЛОГИРОВАНИЕ
file_log = log.FileHandler('QuickImport.log')
console_log = log.StreamHandler()

log.basicConfig(handlers=(file_log, console_log),
            format = u'%(filename)s[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s',
            datefmt='%m/%d/%Y %I:%M:%S %p', level=log.DEBUG)
#log.debug('debug message')
#log.info('info message')
#log.warning('warn message')
#log.error('error message')
#log.critical('critical message')

# очередь данных от сервера
q_out_service = queue.Queue()

ticker = 'YNDX' #В Квике должен быть открыт стакан, а в таблице обезличенных сделок транслироваться тики
ticks=[] #Для примера, список обезличенных сделок
stakan = '' # строка формата '"имя тикера" {bid_price:bid_size,bid_price1:bid_size1...ask_price:-ask_size,ask_price1:-ask_size1}'

def parser (res):
    parse = res.split(" ", 2) #первый элемент - идентификатор события, второй имя тикера
    if parse[0] == '2': # парсинг стакана (событие '2')
        if parse[1] == ticker:
            global stakan
            stakan = parse[2]
    if parse[0] == '1': # парсинг обезличенной сделки (событие '1')
        tail = res.split(" ")
        if tail[1] == ticker: #записываем цену текущего тика BRN0 в список ticks
            ticks.append(float(tail[4]))

def service():
    '''Принимает от сервера данные'''
    sock=socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
    sock.bind((ip, port))
    log.debug('Поток service запущен')
    while True:
        res = sock.recv(2048).decode('utf-8')
        if res == '<qstp>\n':  #строка приходит от клиента при остановке луа-скрипта в КВИКе
            log.warning('Потеряна связь с сервером QLua')
            q_out_service.put(False)
            break
        else:
            q_out_service.put(res)
    log.debug('Поток service остановлен')
    sock.close()

def memory_free():
    '''Возвращает свободную оперативную память в Кб'''
    with open('/proc/meminfo', 'r') as mem:
        for i in mem:
            sline = i.split()
            if str(sline[0]) == 'MemFree:': return int(sline[1])

def db():
    '''Записывает в БД sqlite данные, полученные от сервера'''
    log.debug('Поток db запущен')
    # ищем доступный путь для БД в tmpfs
    temp_file = MemoryTempfile(preferred_paths=['/run/user/{uid}'], remove_paths=['/dev/shm', '/run/shm'],
                              additional_paths=['/var/run'], filesystem_types=['tmpfs'], fallback=True)
    if not temp_file.found_mem_tempdir():
        log.critical('Невозможно создать базу данных. Отсутствует доступный путь до MemoryTempfile.')
        log.info('Завершение работы.')
        os._exit(0)

    # создаем БД
    temp_file = temp_file.gettempdir() + '/' + name_db
    conn = sqlite3.connect(temp_file)
    c = conn.cursor()
    c.execute('''CREATE TABLE IF NOT EXISTS "transaction" (
                post_id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                ticker TEXT NOT NULL,
                price REAL NULL,
                trans TEXT NOT NULL,
                volume REAL NOT NULL,
                date TEXT NOT NULL,
                time TEXT NOT NULL );
                ''')
    time1 = time.time()
    while True:
        data = q_out_service.get() # читаем из очереди
        if data == False: break # обработка стоп-сигнала с клиента service
        #1 - обезличенная сделка
        #2 - биржевой стакан
        if int(data[0]) == 1:
            data = str(data[2:-1]).replace('_SPB','').split(' ') # убираем тип обезличенная сделка,_SPB, пробелы и /n в конце
            # ['REGI', '1049794', '19351405855071396', '58.9', '1025', '4.0', '23.10.2020', '18:55:26.126']
            data[4] = 'buy' if data[4] == 1026 else 'sell' # замена кодов 1026/1025 на buy/sell
            datas = (data[0],float(data[3]),data[4],float(data[5]),data[6],data[7])
            c.execute('''INSERT INTO "transaction" (ticker, price, trans, volume, date, time)
                        VALUES (?,?,?,?,?,?)''',datas)
        time2 = time.time()
        # COMMIT через интервалы времени
        if time2 - time1 >= time_commit_db:
            time1 = time2
            conn.commit()
            # остаток свободной памяти меньше 50%
            if memory_free() - int(os.path.getsize(temp_file))/1024 <= 0.5 * memory_free():
                log.warning(f'Осталось меньше 50% от свободной памяти. БД занимает {os.path.getsize(temp_file)/1024/1024} Мб.')
            # удаление записей больше определенного количества, заданного в volume_db_str
            c.execute('''SELECT * FROM "transaction"''')
            if len(c.fetchall()) > volume_db_str:
                c.execute('''DELETE FROM "transaction" WHERE post_id >= ?''', (volume_db_str,))
                c.execute('UPDATE "transaction" SET post_id = 1')
                log.debug(f'БД очищена.')
                    1111111111




    conn.commit()
    conn.close()
    log.debug('Поток db остановлен')

def main():
    log.info('QuickImport успешно запущен')
    t_service = threading.Thread(name = 'service', target = service)
    t_db = threading.Thread(name = 'db', target = db)
    t_service.start()
    t_db.start()

if __name__ == '__main__':
    if os.name == 'posix': main()
    else: log.critical('\nNo POSIX. Exit...\n')
